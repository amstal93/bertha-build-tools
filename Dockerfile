ARG BASE_IMAGE=docker
FROM ${BASE_IMAGE}

RUN apk --no-cache add \
    bash \
    shadow \
    git \
    make \
    jq \
    rsync \
    openssh \
    curl \
    python3 \
        py3-pip \
        py3-pynacl \
        py3-cryptography \
        py3-bcrypt \
&& \
    pip3 install --no-cache-dir --upgrade pip \
&& true

# Buildx for BuildKit features (mainly parallelization of building stages)
COPY --from=docker/buildx-bin /buildx /usr/libexec/docker/cli-plugins/docker-buildx
COPY --from=docker/compose /usr/local/bin/docker-compose /usr/local/bin/docker-compose

RUN apk add --no-cache \
    --virtual .build-deps \
        python3-dev \
        libffi-dev \
        openssl-dev \
        gcc \
        libc-dev \
    &&\
    pip3 install --no-cache-dir "pylint_runner" &&\
    pip3 install --no-cache-dir "black==19.10b0" &&\
    apk del .build-deps
# Remove pylint_runner install from here when Python3.8 is used
# and place in requirements.txt

# Install terraform (the version in `apk add` is not controlable by us)
COPY --from=hashicorp/terraform /bin/terraform /bin/terraform

COPY requirements.txt requirements.txt
RUN pip3 install --no-cache-dir -r requirements.txt

WORKDIR /bertha-build-tools/
COPY setup.py ./
COPY bertha-build-tools/ bertha-build-tools/
RUN pip3 install .
