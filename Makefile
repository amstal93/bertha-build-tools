CI_COMMIT_SHORT_SHA  ?= $(shell git rev-parse HEAD | cut -c 1-8)
CI_REGISTRY_IMAGE ?= registry.gitlab.com/genomicsengland/opensource/bertha-build-tools

build:
	docker build --pull \
		--tag ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA} \
		--tag ${CI_REGISTRY_IMAGE}:latest \
	.

run:
	docker run --rm -it \
		${CI_REGISTRY_IMAGE}:latest \
		/bin/bash
